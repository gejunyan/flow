const express = require("express");
const router = express.Router();
const flowdef = require("../flowdefinition")
const Queue = require("bull");
const GUI = require('bull-arena');
const QUEUE_NAME= 'FLOW';
const REDIS_URL='redis://h:zwWbvx0uyH2ZYceqMAUzeHXm8u90ROnK@redis-14881.c1.asia-northeast1-1.gce.cloud.redislabs.com:14881'
const flowQueue = new Queue(QUEUE_NAME, REDIS_URL);

const queueDashboard = GUI({
	queues: [
		{
			name: QUEUE_NAME,
			hostId: "flow",
			url: REDIS_URL
		}
	]
}, {
	disableListen: true
});


module.exports = app => {
  router.use('/', queueDashboard)
  app.use('/queue_dashboard', router);


  app.get('/register', function(req, res) {
    // HardCode Scenario:
    //flowQueue.add({ jsonObj: flowdef.jsonobj });

    // Scenario 1: let say we can pass mongo ID
    // flowQueue.add({ _id: 'some mongo ID' });

    // Scenario 2: let say we have multi tenant, 1 mongo and multiple database
    // flowQueue.add({ database: 'sony', _id: 'some mongo ID' });

    // Scenario 3: let say we have multi tenant, 1 mongo and multiple database
    // flowQueue.add({ database_url: 'mongod://localhost/flow', _id: 'some mongo ID'});
    
  	flowQueue.add({ jsonObj: flowdef.jsonobj });
  	res.send('Success');
  })

  app.get('/user_response', function(req, res) {
  	// update mongo
  	flowQueue.add({ getUserResponse });
  	res.send('Success');
  })
}
