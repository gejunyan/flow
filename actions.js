var logHistory = (message) => {
  console.log(message)
}

var addme = (b, c) => {
  return b + c
}

module.exports = {
  logHistory: logHistory,
  addme: addme
}
