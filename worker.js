const flowrunner = require("./flowrunner")
const Queue = require("bull");

const QUEUE_NAME= 'FLOW';
const REDIS_URL='redis://h:zwWbvx0uyH2ZYceqMAUzeHXm8u90ROnK@redis-14881.c1.asia-northeast1-1.gce.cloud.redislabs.com:14881' //'redis://127.0.0.1:6379'

const flowQueue = new Queue(QUEUE_NAME, REDIS_URL);

flowQueue.process(function(job, done) {
	let jsonObj = job.data.jsonObj

	// query mongo for data
	flowrunner.startflow(jsonObj);
	done();
});

// Execute a flow definition's actions
