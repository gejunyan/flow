// flow actions/activities/functions examples to be implemented in it's own module and imported here
const JSONPath = require("jsonpath");
const actions = require("./actions");

// flow variable(s)
const varVault = {}
var newDate = new Date();

//exec first node first, and recurse
const exec1 = (array) => {
  if (array.length > 0) {
    var first = array.shift()
    switch (first.type) {
      case "IF_ELSE":
        if (eval(first.conditions[0].condition)) {
          //exec1(first.branches[1].actions)
          exec1(JSONPath.query(first, '$..branches[?(@.condition==true)].actions')[0])
        } else {
          //exec1(first.branches[0].actions)
          exec1(JSONPath.query(first, '$..branches[?(@.condition==false)].actions')[0])
        }
        break
      case "IF_TRUE":
        if (eval(first.conditions[0].condition)) {
          exec1(first.branches[0].actions)
        }
        break
      case "WHILE":
        while (eval(first.conditions[0].condition)) {
          var temparray = first.actions.slice()
          exec1(temparray)
        }
        break
      case "FOR_OF":
        for (let x of varVault[first.params.array]) {
          var temparray = first.actions.slice()
          varVault[first.params.variable] = x;
          exec1(temparray)
        }
        break
      case "func":
        eval(first.func)
        console.log(new Date().toLocaleString(), ": ", first.title, " - ", first.func)
        break
      default:
        console.log(new Date().toLocaleString(), ": ", first.title)
        break
    }
    exec1(array)
  }
}

var startflow = (flowdef) => {
  flowdef.variables.forEach(function(e) {
    varVault[e.name] = e.value
    //console.log(e.name, "=", e.value, ";", varVault[e.name])
  })
  exec1(flowdef.actions)
}

module.exports = {
  startflow: startflow
}
