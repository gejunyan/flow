// sample flow definition
const varVault = {}
const jsonobj = {
  variables: [
    {"name":"ABC", "type":"integer", "value": 4},
    {"name":"days","type":"array","value":["Monday","Tuesday","Wednesday"]}
  ],
  actions: [
    {"title":"Action 0", "name":"func", "type":"func", "func":"actions.logHistory('Flow started')"},
    {"title":"Action 1", "name":"my name", "type":"display", "a":"a"},
    {"title":"Action 2", "name":"my name", "type":"display", "b":2},
    {"title":"Action 3", "name":"func", "type":"func", "func":"actions.logHistory('We are at action 2.1, ' + varVault.ABC)"},
    {"title":"Action 4", "name":"IF", "type":"IF_ELSE",
      "conditions": [{condition: "varVault.ABC == 3"}],
      "branches":[
		    {condition:false, "actions":[
				      {"title":"Action 4.1.1", "name":"c.no.1", "type":"display", "c.no.1":""},
				      {"title":"Action 4.1.2", "name":"c.no.2", "type":"display", "c.no.2":""}
			  ]},
        {condition:true, "actions":[
				      {"title":"Action 4.2.1", "name":"c.yes.1", "type":"display", "c.yes.1":""},
				      {"title":"Action 4.2.2", "name":"c.yes.2", "type":"display", "c.yes.2":""}
			  ]}
      ]
    },
    {"title":"Action 5", "name":"my name", "type":"display", "d":"asdf"},
    {"title":"Action 6", "name":"WHILE", "type" : "WHILE",
      "conditions": [{condition: "varVault.ABC < 8"}],
      "actions":[
			     {"title":"Action 6.1", "name": "func", "type":"func", "func": "actions.addme(varVault.ABC, 1)"},
			     {"title":"Action 6.2", "name": "func", "type":"func", "func": "varVault.ABC = actions.addme(varVault.ABC, 1)"}
		  ]
    },
    {"title":"Action 7", "name":"my name", "type":"display", e:"asdf"},
    {"title":"Action 8", "name":"func", "type":"func", "func":"actions.logHistory('Flow completed')"},
    {"title":"Action 9", "name":"FOR_OF", "type":"FOR_OF",
      "params":{"variable": "x", "array": "days"},
      "actions":[
        {"title":"Atcion 9.1", "name":"show x", "type":"func", "func": "actions.logHistory('varVault.x = ' + varVault.x)"}
      ]
    }
  ]
}

module.exports = {
  jsonobj: jsonobj
}
